import React, { Component } from 'react';
import {Form, Row, Col, Button, Modal} from 'react-bootstrap';
import '../style/search.css'
import axios from "axios";

class Emails extends Component {

    constructor(props) {
        super(props);
        this.state = {emails: [], show: false, searchSubject: '', searchContent: '', searchTO:'', searchFrom:'',
         searchAccount:'', searchFolder:'', searchPDF: '', subject:this.props.subject, content:this.props.content, dateTime:this.props.dateTime,
        from:this.props.from, folder: this.props.folder, attachment:this.props.attachment, attachmentName:'',
        username: this.props.username, password: this.props.password, firstName:this.props.firstName, lastName:this.props.lastName, showUpdatePersonalData:false}
        this.getEmails();
    }

    reset(){
        this.getEmails()
    }

    getEmails(){
        var emails_list;
        let headers = {
           headers: {
             'Authorization': 'Bearer ' + this.props.location.state.token,
             'Access-Control-Allow-Origin':'*'
           }
        }
        axios.get('http://localhost:8080/api/inbox/emails', headers)
        .then((response) => {
           emails_list = response.data;
           console.log(emails_list)
           this.setState((prevState) => {
              return {
                 emails: emails_list
              }
           })
        });
    }

    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    startContact(){
        this.props.history.push({
            pathname: '/contacts',
            state: { token: this.props.location.state.token }
        })
    }
    search(){
        var url;
        var field;
        var value;
        var params;

        if (this.refs.operator) {
            console.log(this.refs.operator.value);
        }

        if(this.state.searchSubject.trim()){
            if(this.state.searchSubject.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/message/phrase"
            }else{
                url = "http://localhost:8080/api/search/message/term"
            }
            params = {
                value:this.state.searchSubject.trim(),
                field:'subject',
            }
        }

        if(this.state.searchContent.trim()){
            if(this.state.searchContent.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/message/phrase"
            }else{
                url = "http://localhost:8080/api/search/message/term"
            }
            params = {
                value:this.state.searchContent.trim(),
                field:'content',
            }
        }

        if(this.state.searchFrom.trim()){
            url = "http://localhost:8080/api/search/message/nested"
            params = {
                value:this.state.searchFrom.trim(),
                field:'from.email'
            }
        }

        if(this.state.searchAccount.trim()){
            if(this.state.searchAccount.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/message/nested"
            }else{
                url = "http://localhost:8080/api/search/message/nested"
            }
            params = {
                value:this.state.searchAccount.trim(),
                field:'account.username'
            }
        }

        if(this.state.searchFolder.trim()){
            url = "http://localhost:8080/api/search/message/nested"
            params = {
                value:this.state.searchFolder.trim(),
                field:'folder.name'
            }
        }

        if(this.state.searchPDF.trim()){
            url = "http://localhost:8080/api/search/message/phrase/attach"
            params = {
                value:this.state.searchPDF.trim(),
                field:'content',
            }
        }

        if(this.state.searchSubject.trim() && this.state.searchContent.trim()){
            params = {
                value1:this.state.searchSubject.trim(),
                value2:this.state.searchContent.trim(),
                field1:'subject',
                field2:'content',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/search-boolean"
        }

        if(this.state.searchSubject.trim() && this.state.searchFrom.trim()){
            params = {
                value1:this.state.searchSubject.trim(),
                value2:this.state.searchFrom.trim(),
                field1:'subject',
                field2:'from.email',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/search-boolean"
        }

        if(this.state.searchContent.trim() && this.state.searchFrom.trim()){
            params = {
                value1:this.state.searchContent.trim(),
                value2:this.state.searchFrom.trim(),
                field1:'content',
                field2:'from.email',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/search-boolean"
        }

        if(this.state.searchSubject.trim() && this.state.searchPDF.trim()){
            params = {
                value1:this.state.searchSubject.trim(),
                value2:this.state.searchPDF.trim(),
                field1:'message.subject',
                field2:'content',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/bool/attach"
        }

        if(this.state.searchContent.trim() && this.state.searchPDF.trim()){
            params = {
                value1:this.state.searchContent.trim(),
                value2:this.state.searchPDF.trim(),
                field1:'message.content',
                field2:'content',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/bool/attach"
        }

        if(this.state.searchFrom.trim() && this.state.searchPDF.trim()){
            params = {
                value1:this.state.searchFrom.trim(),
                value2:this.state.searchPDF.trim(),
                field1:'message.from.email',
                field2:'content',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/message/bool/attach"
        }

        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
         }

        axios.post(url,params, headers)
        .then((response) => {
            const res_obj = response.data;
            console.log(res_obj)
            this.setState(() => {
                return {
                   emails: res_obj
                }
             })
         });
    }

    renderTableData() {
        return this.state.emails.map((email, index) => {
           const { id, dateTime, subject, content, from, folder, read} = email
           return (
              <tr class="unread" key={id} onClick={() => {this.handleUpdateModal(id, dateTime, subject, content, from, folder)}}>
                <td class="view-message  dont-show">{from.email}</td>
                <td class="view-message"><b>{subject}</b></td>
                <td class="view-message" style={{whiteSpace:'nowrap', overflow:'hidden', textOverflow:'ellipsis', width:'100px', maxWidth: '400px'}}>{content}</td>
                <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td>
                <td class="view-message  text-right">{dateTime}</td>
              </tr>

           )
        })
    }

    handleUpdateModal(id, dateTime, subject, content, from){
        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
         }
         axios.get('http://localhost:8080/api/emails/get-attachment/' + id, headers)
         .then((response) => {
            console.log(response.data.name)
            if(response.data.name.trim()){
                this.setState({attachmentName:response.data.name})
                var button = document.createElement("input");
                button.type = "button";
                button.value = response.data.name;
              
                var selectPanel = document.getElementById('attachment-div');
                selectPanel.appendChild(button);
            }
         });
        this.setState({show: !this.state.show, subject:subject, content:content, from:from.email, dateTime:dateTime});
    }

    handleSubjectChange(event) {
       this.setState({subject: event.target.value});
    }

    handleContentChange(event) {
        this.setState({content: event.target.value});
    }

    handleFromChange(event) {
        this.setState({from: event.target.value});
    }

    handleDateTimeChange(event) {
        this.setState({dateTime: event.target.value});
    }

    handleUsernameChange(event) {
        this.setState({username: event.target.value});
    }    
  
    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    handleFirstNameChange(event) {
        this.setState({firstName: event.target.value});
    }

    handleLastNameChange(event) {
        this.setState({lastName: event.target.value});
    }
    
	downloadFile(){
        setTimeout(() => {
            const response = {
            file: 'http://localhost:8080/api/message/' + this.state.attachmentName,
            };
            window.location.href = response.file;
        }, 100);
    }

    handleUpdatePersonalData(){
        this.profile();
        this.setState({showUpdatePersonalData: !this.state.showUpdatePersonalData});
    }

    profile(){
        var user;
        let headers = {
           headers: {
             'Authorization': 'Bearer ' + this.props.location.state.token,
             'Access-Control-Allow-Origin':'*'
           }
        }
        axios.get('http://localhost:8080/api/user/get-current', headers)
        .then((response) => {
           user = response.data;
           this.setState((prevState) => {
              return {
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName
                }
            });
        });
        console.log(this.state.username)
    }

    updatePersonalData(){
        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
        }

            if(this.state.password.trim() != ''){
                var params = {
                    username:this.state.username,
                    password:this.state.password,
                    firstName:this.state.firstName,
                    lastName:this.state.lastName
                }
                axios.put("http://localhost:8080/api/user/changePassword",params, headers)
                .then((response) => {
                    const res_obj = response.data;
                    alert('Password successfully changed, please login with new password!')
                    this.props.history.push({
                        pathname: '/',
                        state: { token: null }
                    })
                });        
            }
    }

    logout(){
        this.props.history.push({
            pathname: '/',
            state: { token: null }
        })
    }
    
    render(){
        const { searchSubject, searchContent, searchFolder, searchFrom, searchAccount, searchPDF} = this.state;
        return(
            <React.Fragment>
                <nav style={{backgroundColor: 'red', width: '100vw',paddingTop:'0.4vw', backgroundColor: '#D6EFFF', height:'3vw'}}>
                    <ul style={{margin: '0', padding: '0', display: 'flex'}}>
                    <li style={{listStyleType: 'none',  margin: '0 1vw', fontSize: '3vh'}}>
                            <a style={{textDecoration: 'none', padding: '1vw', fontFamily: 'monospace'}}><button style={{backgroundColor:'white'}}>HOME</button></a>
                        </li>
                        <li style={{listStyleType: 'none', margin: '0 1vw', fontSize: '3vh'}}>
                            <a style={{textDecoration: 'none', padding: '1vw', fontFamily: 'monospace'}}><button style={{backgroundColor:'white'}} onClick={() => {this.startContact()}}>CONTACTS</button></a>
                        </li>
                        <li style={{listStyleType: 'none', margin: '0 1vw', fontSize: '3vh'}}>
                            <a style={{textDecoration: 'none', padding: '1vw', fontFamily: 'monospace'}}><button style={{backgroundColor:'white'}} onClick={() => {this.handleUpdatePersonalData()}}>PROFILE</button></a>
                        </li>
                        <li style={{listStyleType: 'none', margin: '0 1vw', fontSize: '3vh'}}>
                            <a style={{textDecoration: 'none', padding: '1vw', fontFamily: 'monospace'}}><button style={{backgroundColor:'white'}} onClick={() => {this.logout()}}>LOG OUT</button></a>
                        </li>
                    </ul>
                </nav>                
                <div id="search-container" className="ml-4 mr-4 mt-2 mb-2">
                <Modal show={this.state.showUpdatePersonalData} onHide={() => {this.handleUpdatePersonalData()}} aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header><b>Personal data</b></Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group>
                            <Form.Label>Username</Form.Label>
                            <Form.Control className="form-control" type="text" value={this.state.username} onChange={this.handleUsernameChange.bind(this)} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Password</Form.Label>
                            <Form.Control className="form-control" type="text" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>First name</Form.Label>
                            <Form.Control className="form-control" type="text" value={this.state.firstName} onChange={this.handleFirstNameChange.bind(this)} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Last name</Form.Label>
                            <Form.Control className="form-control" type="text" value={this.state.lastName} onChange={this.handleLastNameChange.bind(this)} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button type="button" className="btn btn-lg btn-danger" onClick={() => {this.handleUpdatePersonalData()}}>Close</Button>
                    <Button type="button" className="btn btn-lg btn-success" onClick={() => {this.updatePersonalData()}}>Update personal data</Button>
                </Modal.Footer>
            </Modal>
            <Modal show={this.state.show} onHide={() => {this.handleUpdateModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
               <Modal.Header><Form.Control className="form-control" disabled type="text" value={this.state.subject} onChange={this.handleSubjectChange.bind(this)} /></Modal.Header>
               <Modal.Body>
                  <Form>
                    <Form.Group>
                        <Form.Label>From</Form.Label>
                        <Form.Control className="form-control" disabled type="text" value={this.state.from} onChange={this.handleFromChange.bind(this)} />
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Date-time</Form.Label>
                        <Form.Control className="form-control" disabled type="text" value={this.state.dateTime} onChange={this.handleDateTimeChange.bind(this)} />
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Content</Form.Label>
                        <Form.Control className="form-control" disabled type="text" value={this.state.content} onChange={this.handleContentChange.bind(this)} as="textarea" rows="5" />
                     </Form.Group>
                     <Form.Group>
                        <div id="attachment-div" onClick={() => {this.downloadFile(this.state.attachmentName)}}></div>
                     </Form.Group>
                  </Form>
               </Modal.Body>
               <Modal.Footer>
                  <Button type="button" className="btn btn-lg btn-danger"  onClick={() => {this.handleUpdateModal('','','','','')}}>Close</Button>
               </Modal.Footer>
            </Modal>
                <Form>
                <Row>
                    <Col>
                    <Form.Control name="searchSubject" value={searchSubject} onChange={this.changeHandler} className="field" placeholder="Email subject" />
                    <Form.Control name="searchContent" value={searchContent} onChange={this.changeHandler} className="field" placeholder="Email content"/>
                    </Col>
                    <Col>
                    <Form.Control name="searchFrom" value={searchFrom} onChange={this.changeHandler} className="field" placeholder="Email from" />
                    <Form.Control name="searchPDF" value={searchPDF} onChange={this.changeHandler} className="field" placeholder="Attachment content" />
                    </Col>
                    <Col>
                    <Form.Control name="searchFolder" value={searchFolder} onChange={this.changeHandler} className="field" placeholder="Email directory" />
                    <Form.Control name="searchAccount" value={searchAccount} onChange={this.changeHandler} className="field" placeholder="Email account" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <select ref="operator">
                        <option value="NOT">NOT</option>
                        <option value="AND">AND</option>
                        <option value="OR">OR</option>
                    </select>
                    <Button variant="primary" size="lg" className="btn mr-3 ml-3" onClick={() => {this.search()}}>
                    Search
                    </Button>
                    <Button variant="danger" size="lg" className="btn" onClick={() => {this.reset()}}>X</Button>
                    </Col>
                </Row>
                </Form>
            </div>
                <div style={{borderStyle: 'solid', borderColor:'#3399ff', display:'flex'}} class="container-fluid" className="ml-4 mr-4 mb-4">
                    <table className="table table-inbox table-hover" bodyStyle={ {width: 500, maxWidth: 500, wordBreak: 'break-all'}}>
                        <thead style={{backgroundColor:'#D6EFFF'}}>
                            <tr>
                                <th>FROM</th>
                                <th>TITLE</th>
                                <th>CONTENT</th>
                                <th>DATE TIME</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTableData()}
                        </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }
}

export default Emails;