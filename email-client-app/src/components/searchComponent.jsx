import React from "react";
import {Form, Row, Col, Button} from 'react-bootstrap';
import '../style/search.css'

const Search = () => {
  return (
    <React.Fragment>
      <div id="search-container" className="ml-4 mr-4 mt-2 mb-2">
        <Form>
          <Row>
            <Col>
              <Form.Control className="field" placeholder="Email subject" />
              <Form.Control className="field" placeholder="Email content" />
            </Col>
            <Col>
              <Form.Control className="field" placeholder="Email to" />
              <Form.Control className="field" placeholder="Email from" />
            </Col>
            <Col>
              <Form.Control className="field" placeholder="Attachment content" />
              <Form.Control className="field" placeholder="Last name" />
            </Col>
          </Row>
          <Row>
          <Col>
              <Form.Control className="fieldr2" placeholder="Email directory" />
            </Col>
            <Col>
              <Form.Control className="fieldr2" placeholder="Email account" />
            </Col>
            <Col>
              <Button variant="primary" size="lg" className="btn" onClick={() => {this.handleRegModal()}}>
              <i class="fa fa-search"  style={{color:"white"}}></i>
              </Button>
            </Col>
          </Row>
          </Form>
      </div>
    </React.Fragment>
  );
}

export default Search;