import React, { Component } from 'react';
import {Modal, Form, Button} from 'react-bootstrap';
import logo from '../images/emails.png';
import axios from "axios";

class Start extends Component {

    constructor() {
        super();
        this.state = { show: false, registerPassword: '', registerRepeatedPassword: '', registerFirstName:'', registerLastName: '',
                       loginUsername: '', loginPassword: '', registerUsername: ''};
        this.loginState = { show1: false}
    }

    handleRegModal(){
        this.setState({show: !this.state.show})
    }

    handleLoginModal(){
        this.setState({show1: !this.state.show1})
    }

    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    registerClickHandler = (e) => {
        fetch('http://localhost:8080/api/user/add', {  
            method: 'post',  
            headers: {  
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin':'*',
            },  
            body: JSON.stringify({  
                password: this.state.registerPassword,
                username: this.state.registerUsername,
                firstName: this.state.registerFirstName,
                lastName: this.state.registerLastName
            }),
        }).then((Response) => Response.json())  
            .then((result) => {  
                console.log(result);  
                if (result.Status == 'Invalid')  
                    alert('Invalid User');  
            })
    }

    loginClickHandler = (e) => {
        try {
            axios.post('http://localhost:8080/authenticate', { userName: this.state.loginUsername, password: this.state.loginPassword})
            .then((response) => {
                const auth_object = response.data;
                if(auth_object === '')
                    console.log('NEDEFINISANO I NULL')
                else
                    this.props.history.push({
                        pathname: '/accounts',
                        state: { token: auth_object.token }
                    })
            });
        }catch(e){
            console.log(e)
        }
    }

    render() { 
        const { registerUsername, registerPassword, registerRepeatedPassword, registerFirstName, registerLastName, loginUsername, loginPassword } = this.state;
        return (
        <React.Fragment>
            <div style={{ backgroundImage: "url(" + logo +")", height:1000 }}>
                <Modal show={this.state.show1} onHide={() => {this.handleLoginModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header><b>Login - enter username and password </b></Modal.Header>
                    <Modal.Body>
                    <Form>
                        <Form.Group controlId="formGroupUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" name="loginUsername" onChange={this.changeHandler} />
                        </Form.Group>
                        <Form.Group controlId="formGroupPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="loginPassword" onChange={this.changeHandler} />
                        </Form.Group>
                    </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="button" className="btn btn-lg btn-danger" onClick={() => {this.handleLoginModal()}}>Close</Button>
                        <Button type="button" className="btn btn-lg btn-success" onClick = {this.loginClickHandler}>Login</Button>
                    </Modal.Footer>
                </Modal>
                <Modal  show={this.state.show} onHide={() => {this.handleRegModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header><b>Register - enter requested data </b></Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group controlId="formGroupUsername">
                                <Form.Label>Username</Form.Label>
                                <Form.Control type="text" name="registerUsername" value={registerUsername} onChange={this.changeHandler} />
                            </Form.Group>
                            <Form.Group controlId="formGroupPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="registerPassword" value={registerPassword} onChange={this.changeHandler} />
                            </Form.Group>
                            <Form.Group controlId="formGroupRepeatedPassword">
                                <Form.Label>Repeated password</Form.Label>
                                <Form.Control type="password" name="registerRepeatedPassword" value={registerRepeatedPassword} onChange={this.changeHandler} />
                            </Form.Group>
                            <Form.Group controlId="formGroupFirstName">
                                <Form.Label>First name</Form.Label>
                                <Form.Control type="text" name="registerFirstName" value={registerFirstName} onChange={this.changeHandler} />
                            </Form.Group>
                            <Form.Group controlId="formGroupLastName">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control type="text" name="registerLastName" value={registerLastName} onChange={this.changeHandler} />
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="button" className="btn btn-lg btn-danger"  onClick={() => {this.handleRegModal()}}>Close</Button>
                        <Button type="button" className="btn btn-lg btn-success" onClick={this.registerClickHandler}>Register</Button>
                    </Modal.Footer>
                </Modal>
                <p style={{display: 'flex', fontFamily: 'arial', fontSize:90, color:'white', justifyContent:'center', alignItems:'center', height: '90vh'}}>EMAIL CLIENT NUMBER ONE IN THE WORLD</p>
                <Button type="button" style={{height: 90, width: 150, fontSize: 36}} size="lg" className="btn btn-dark btn-outline-light mr-5" onClick={() => {this.handleLoginModal()}}>Login</Button>
                <Button type="button" style={{height: 90, width: 150, fontSize: 36}} size="lg" className="btn btn-dark btn-outline-light mr-5" onClick={() => {this.handleRegModal()}}>Register</Button>
            </div>
        </React.Fragment>
        );
    }
}

export default Start;