import React, { Component } from 'react';
import {Dropdown, Modal, Form, Button} from 'react-bootstrap';
import { Row, Col, } from 'react-bootstrap';
import axios from "axios";

class Contacts extends Component {

    constructor(props) {
        super(props);
        this.state = {contacts:[], token:'', id:0, operation:'', firstName: "", lastName:"", email:"", note:"", showUpdate: false, firstNameCreate: '',
                lastNameCreate:'', emailCreate:'', noteCreate:'', displayNameCreate:''};
        this.call()
    }

    ree(){}

    search(){
        var url;
        var field;
        var value;
        var field1;
        var value1;
        var field2;
        var value2;
        var params;


        if (this.refs.operator) {
            console.log(this.refs.operator.value);
        }

        if(this.state.firstName.trim()){
            if(this.state.firstName.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/contact/phrase"
            }else{
                url = "http://localhost:8080/api/search/contact/term"
            }
            params = {
                value:this.state.firstName.trim(),
                field:'firstName',
            }
        }
        if(this.state.lastName.trim()){
            if(this.state.lastName.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/contact/phrase"
            }else{
                url = "http://localhost:8080/api/search/contact/term"
            }
            params = {
                value:this.state.lastName,
                field:"lastName"
            }
        }
        if(this.state.note.trim()){
            if(this.state.note.trim().indexOf(' ') >= 0){
                url = "http://localhost:8080/api/search/contact/phrase"
            }else{
                url = "http://localhost:8080/api/search/contact/term"
            }
            params = {
                value:this.state.note,
                field:"note"
            }
        }

        if(this.state.firstName.trim() && this.state.lastName.trim()){
            params = {
                value1:this.state.firstName.trim(),
                value2:this.state.lastName.trim(),
                field1:'firstName',
                field2:'lastName',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/contact/search-boolean"
        }

        if(this.state.firstName.trim() && this.state.note.trim()){
            params = {
                value1:this.state.firstName.trim(),
                value2:this.state.note.trim(),
                field1:'firstName',
                field2:'note',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/contact/search-boolean"
        }

        if(this.state.lastName.trim() && this.state.note.trim()){
            params = {
                value2:this.state.lastName.trim(),
                value1:this.state.note.trim(),
                field2:'lastName',
                field1:'note',
                operation: this.refs.operator.value
            }
            url = "http://localhost:8080/api/search/contact/search-boolean"
        }

        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
         }
         axios.post(url,params, headers)
         .then((response) => {
            const res_obj = response.data;
            console.log(res_obj)
            this.setState(() => {
                return {
                    contacts: res_obj
                }
             })
         });
    }

    handleUpdateModal(id, firstName, lastName, email, note, operation){
        console.log(operation + " evooo")
        this.setState({showUpdate: !this.state.showUpdate, id:id, firstNameCreate:firstName, lastNameCreate:lastName, emailCreate:email, noteCreate:note, operation:operation});
    }

    updateClickHandler = (e) => {
        console.log('Note: ' + this.state.noteCreate)
        console.log('First name: ' + this.state.firstNameCreate)
        console.log('Last name: ' + this.state.lastNameCreate)
        console.log('Display name: ' + this.state.displayNameCreate)
        console.log('Email: ' + this.state.emailCreate)
        console.log('Note: ' + this.state.noteCreate)
        console.log('ID: ' + this.state.id)

        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
        }
        if(this.state.operation === 'create'){
            try {
                axios.post('http://localhost:8080/api/contacts/add',  { firstName: this.state.firstNameCreate, lastName: this.state.lastNameCreate,
                                     displayName: this.state.displayNameCreate, email: this.state.emailCreate, note: this.state.noteCreate}, headers)
                .then((response) => {
                    const obj = response.data;
                    window.location.reload();
                });
            }catch(e){
                console.log(e)
            }
        }
        if(this.state.operation === 'update'){
            try {
                axios.put('http://localhost:8080/api/contacts/',  {id:this.state.id, firstName: this.state.firstNameCreate, lastName: this.state.lastNameCreate,
                                     displayName: this.state.displayNameCreate, email: this.state.emailCreate, note: this.state.noteCreate}, headers)
                .then((response) => {
                    const obj = response.data;
                    window.location.reload();
                });
            }catch(e){
                console.log(e)
            }
        }
    }

    reset(){
        this.call();
    }

    call(){
        this.setState(() => {
            return {
                token: this.props.location.state.token
            }
         })
        let headers = {
            headers: {
              'Authorization': 'Bearer ' + this.props.location.state.token,
              'Access-Control-Allow-Origin':'*'
            }
         }
         axios.get('http://localhost:8080/api/contacts', headers)
         .then((response) => {
            const account_obj = response.data;
            console.log(account_obj)
            this.setState((prevState) => {
                return {
                    contacts: account_obj
                }
             })
         });
    }

    handleDeleteModal(id){
        this.setState({show: !this.state.show});
        this.setState({toDelete: id});
     }

    renderTableData() {
        return this.state.contacts.map((contact, index) => {
           const { id, firstName, lastName, email, note} = contact
           return (
              <tr class="unread" key={id} onClick={() => {this.ree()}}>
                <td class="view-message  dont-show">{firstName}</td>
                <td class="view-message">{lastName}</td>
                <td class="view-message  text-right">{email}</td>
                <td><Button onClick={() => {this.handleUpdateModal(id, firstName, lastName, email, note, "update")}} variant="primary">UPDATE</Button></td>
                <td><Button variant="danger" onClick={() => {this.handleDeleteModal(id)}}>X</Button></td>
              </tr>
           )
        })
    }
    handleFirstNameChange(event) {
        this.setState({firstNameCreate: event.target.value});
    }    

    handleLastNameChange(event) {
        this.setState({lastNameCreate: event.target.value});
    } 

    handleEmailChange(event) {
        this.setState({emailCreate: event.target.value});
    } 

    handleDisplayNameChange(event) {
        this.setState({displayNameCreate: event.target.value});
    } 

    handleNoteChange(event) {
        this.setState({noteCreate: event.target.value});
    } 

    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }
    
    deleteAccount(){
        let headers = {
           headers: {
             'Authorization': 'Bearer ' + this.props.location.state.token,
             'Access-Control-Allow-Origin':'*'
           }
        }
        axios.delete('http://localhost:8080/api/contacts/' + this.state.toDelete, headers)
        .then((response) => {
           const a = response.data;
           this.handleDeleteModal(null)
           window.location.reload();
        });
    }

    render() { 
        const { firstName, lastName, note} = this.state;
        return (
        <React.Fragment>
            <nav style={{backgroundColor: 'red', width: '100vw',paddingTop:'0.4vw', backgroundColor: '#D6EFFF', height:'3vw'}}>
                <ul style={{margin: '0', padding: '0', display: 'flex'}}>
                    <li style={{listStyleType: 'none', margin: '0 1vw', fontSize: '3vh'}}>
                        <a style={{textDecoration: 'none', padding: '1vw', fontFamily: 'monospace'}} onClick={() => {this.handleUpdateModal('','','','','','create')}}><button style={{backgroundColor:'white'}}>Add new contact</button></a>
                    </li>
                </ul>
            </nav>     
            <div id="search-container" className="ml-4 mr-4 mt-2 mb-2">
                <Form>
                <Row>
                    <Col>
                    <Form.Control name="firstName" value={firstName} onChange={this.changeHandler} className="field" placeholder="First name" />
                    <select ref="operator">
                        <option value="NOT">NOT</option>
                        <option value="AND">AND</option>
                        <option value="OR">OR</option>
                    </select>
                    </Col>
                    <Col>
                    <Form.Control name="lastName" value={lastName} onChange={this.changeHandler} className="field" placeholder="Last name" />
                    <Button variant="primary" size="lg" className="btn mr-3" onClick={() => {this.search()}}>Search</Button>
                    <Button variant="danger" style={{border:"2px solid blue", backgroundColor:"white", color:"blue"}} size="lg" className="btn" onClick={() => {this.reset()}}>X</Button>
                    </Col>
                    <Col>
                    <Form.Control name="note" value={note} onChange={this.changeHandler} className="field" placeholder="Note" />
                    </Col>
                </Row>
                </Form>
            <Modal show={this.state.showUpdate} onHide={() => {this.handleUpdateModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
               <Modal.Header><b>Register - enter requested data </b></Modal.Header>
               <Modal.Body>
                  <Form>
                     <Form.Group>
                        <Form.Label>First name</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.firstNameCreate} onChange={this.handleFirstNameChange.bind(this)}/>
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Last name</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.lastNameCreate} onChange={this.handleLastNameChange.bind(this)}/>
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Diplay name</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.displayNameCreate} onChange={this.handleDisplayNameChange.bind(this)}/>
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Email</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.emailCreate} onChange={this.handleEmailChange.bind(this)}/>
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Note</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.noteCreate} onChange={this.handleNoteChange.bind(this)}/>
                     </Form.Group>
                  </Form>
               </Modal.Body>
               <Modal.Footer>
                  <Button type="button" className="btn btn-lg btn-danger"  onClick={() => {this.handleUpdateModal('','','','','','')}}>Close</Button>
                  <Button type="button" className="btn btn-lg btn-success" onClick={this.updateClickHandler}>Update</Button>
               </Modal.Footer>
            </Modal>
            <Modal show={this.state.show} onHide={() => {this.handleDeleteModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
               <Modal.Header><b>Remove</b></Modal.Header>
               <Modal.Body>
                  Are you sure that you want to delete selected account?
               </Modal.Body>
               <Modal.Footer>
                  <Button type="button" className="btn btn-lg btn-primary" onClick={() => {this.handleDeleteModal()}}>Close</Button>
                  <Button type="button" className="btn btn-lg btn-danger" onClick={() => {this.deleteAccount()}}>Yes</Button>
               </Modal.Footer>
            </Modal>
            </div>
                <div style={{borderStyle: 'solid', borderColor:'#3399ff', display:'flex'}} class="container-fluid" className="ml-4 mr-4">
                    <table class="table table-inbox table-hover" bodyStyle={ {width: 500, maxWidth: 500, wordBreak: 'break-all'}}>
                        <thead style={{backgroundColor:'#D6EFFF'}}>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTableData()}
                        </tbody>
                    </table>
                </div>
        </React.Fragment>
        );
    }
}

export default Contacts;