import React, { Component } from 'react';
import axios from 'axios';
import {Table, Button, Modal, Form} from 'react-bootstrap';

class Accounts extends Component {

   constructor(props) {
      super(props);
      this.state = {accounts: [], show: false, showUpdate: false, toDelete: 0, id:this.props.id, username: this.props.username, password: this.props.password, pop3imap: this.props.pop3imap, smtp: this.props.smtp}
      this.getAccountsReq();
   }

   getAccountsReq(){
      var accounts_list;
      let headers = {
         headers: {
           'Authorization': 'Bearer ' + this.props.location.state.token,
           'Access-Control-Allow-Origin':'*'
         }
      }
      axios.get('http://localhost:8080/api/accounts', headers)
      .then((response) => {
         accounts_list = response.data;
         this.setState((prevState) => {
            return {
               accounts: accounts_list
            }
         })
      });
   }

   handleDeleteModal(id){
      this.setState({show: !this.state.show});
      this.setState({toDelete: id});
   }

   handleUpdateModal(id, smtp, pop3imap, username, password){
      this.setState({showUpdate: !this.state.showUpdate, id:id, username:username, password:password, smtp:smtp, pop3imap:pop3imap });
   }

   deleteAccount(){
      let headers = {
         headers: {
           'Authorization': 'Bearer ' + this.props.location.state.token,
           'Access-Control-Allow-Origin':'*'
         }
      }
      axios.delete('http://localhost:8080/api/accounts/' + this.state.toDelete, headers)
      .then((response) => {
         const a = response.data;
         this.handleDeleteModal(null)
         window.location.reload();
      });
   }

   handleUsernameChange(event) {
      this.setState({username: event.target.value});
   }    

   handlePasswordChange(event) {
      this.setState({password: event.target.value});
   }

   handlePop3ImapChange(event) {
      this.setState({pop3imap: event.target.value});
   }
   
   handleSMTPChange(event) {
      this.setState({smtp: event.target.value});
   }

   handleIDChange(event) {
      this.setState({id: event.target.value});
   }


   updateClickHandler = (e) => {
      let headers = {
         headers: {
           'Authorization': 'Bearer ' + this.props.location.state.token,
           'Access-Control-Allow-Origin':'*'
         }
      }
      try {
         axios.put('http://localhost:8080/api/account/update',  { id: this.state.id, username: this.state.username, password: this.state.password,
                              smtp: this.state.smtp, pop3imap: this.state.pop3imap}, headers)
         .then((response) => {
             const obj = response.data;
             window.location.reload();
         });
     }catch(e){
         console.log(e)
     }
   }

   chooseAccountClickHandler(id) {
      let headers = {
         headers: {
           'Authorization': 'Bearer ' + this.props.location.state.token,
           'Access-Control-Allow-Origin':'*'
         }
      }
      axios.put('http://localhost:8080/api/accounts/set',{id:id}, headers)
      .then((response) => {
         const account_obj = response.data;
         if(account_obj === '')
             console.log('NEDEFINISANO I NULL')
         else
             this.props.history.push({
                 pathname: '/emails',
                 state: { token: this.props.location.state.token }
             })
      });

   }


   renderTableData() {
      return this.state.accounts.map((account, index) => {
         const { id, smtp, pop3imap, username, password } = account
         return (
            <tr key={id}>
               <td>{smtp}</td>
               <td>{pop3imap}</td>
               <td>{username}</td>
               <td>{password}</td>
               <td><Button onClick={() => {this.handleDeleteModal(id)}} variant="danger">REMOVE</Button></td>
               <td><Button onClick={() => {this.handleUpdateModal(id, smtp, pop3imap, username, password)}} variant="primary">UPDATE</Button></td>
               <td><Button onClick={() => {this.chooseAccountClickHandler(id)}} variant="warning">LET'S START WITH THIS ACCOUNT</Button></td>
            </tr>
         )
      })
   }
  
   render() {
      return (
         <React.Fragment>
            <div>
               <Table striped bordered hover variant="dark">
                  <thead>
                     <tr>
                        <th>SMTP</th>
                        <th>POP3IMAP</th>
                        <th>USERNAME</th>
                        <th>PASSWORD</th>
                     </tr>
                  </thead>
                  <tbody>
                     {this.renderTableData()}
                  </tbody>
               </Table>
            </div>
            <Modal show={this.state.show} onHide={() => {this.handleDeleteModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
               <Modal.Header><b>Remove</b></Modal.Header>
               <Modal.Body>
                  Are you sure that you want to delete selected account?
               </Modal.Body>
               <Modal.Footer>
                  <Button type="button" className="btn btn-lg btn-primary" onClick={() => {this.handleDeleteModal()}}>Close</Button>
                  <Button type="button" className="btn btn-lg btn-danger" onClick={() => {this.deleteAccount()}}>Yes</Button>
               </Modal.Footer>
            </Modal>
            <Modal show={this.state.showUpdate} onHide={() => {this.handleUpdateModal()}} aria-labelledby="contained-modal-title-vcenter" centered>
               <Modal.Header><b>Register - enter requested data </b></Modal.Header>
               <Modal.Body>
                  <Form>
                     <Form.Group>
                        <Form.Label>Username</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.username} onChange={this.handleUsernameChange.bind(this)} />
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.password} onChange={this.handlePasswordChange.bind(this)} />
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>SMTP</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.smtp} onChange={this.handleSMTPChange.bind(this)} />
                     </Form.Group>
                     <Form.Group>
                        <Form.Label>POP3IMAP</Form.Label>
                        <Form.Control className="form-control" type="text" value={this.state.pop3imap} onChange={this.handlePop3ImapChange.bind(this)} />
                     </Form.Group>
                  </Form>
               </Modal.Body>
               <Modal.Footer>
                  <Button type="button" className="btn btn-lg btn-danger"  onClick={() => {this.handleUpdateModal('','','','','')}}>Close</Button>
                  <Button type="button" className="btn btn-lg btn-success" onClick={this.updateClickHandler}>Update</Button>
               </Modal.Footer>
            </Modal>
            <Button variant="primary" size="lg" block onClick={() => {this.handleUpdateModal('', '', '', '', '')}}>Add new account</Button>
         </React.Fragment>
      )
   }
}

export default Accounts;