import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Start from './components/startComponent';
import Accounts from './components/accountComponent';
import Emails from './components/emailsComponent'
import Contacts from './components/contactsComponent'

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Start}></Route>
          <Route path="/accounts" exact component={Accounts}></Route>
          <Route path="/emails" exact component={Emails}></Route>
          <Route path="/contacts" exact component={Contacts}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
